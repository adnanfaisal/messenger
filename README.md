Messenger Web Service version 1.0

* Quick summary:
This project contains a set of RESTful web services written in Jersey (JAX-RS). Tomcat Server was used as the web server. This is a facebook-like project where new profiles can be created, members can post messages and comments. The classes that has most features are MessageService and MessageResource. Many features of JAX-RS has been implemented such as Pagination and filtering, Param annotations, Context and BeanParam, Subresource, Status code, Location header, Exception Handling, WebApplicationException, HATEOAS and Content negotiation. You can try out the features using any client such as postman. 

The project uses a layered architecture for its different classes. The Models are in Model package, the Resource Managers are in Resource package and the Services are in Service package.

The project contains only server side code. You may use POSTMAN or other client side application to test the project.