package org.adnan.carleton.exception;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.adnan.carleton.messenger.model.ErrorMessage;

//Commenting out the provider annotation so that WebApplicationException can work
//@Provider
public class GenericExceptionMapper implements ExceptionMapper<Throwable>{

	@Override
	public Response toResponse(Throwable ex) {
		ErrorMessage em = new ErrorMessage(ex.getMessage(), 500, 
				"http://bitbucket.org/adnanfaisal");
		return Response.status(Status.INTERNAL_SERVER_ERROR).
				entity(em).build();
	}
}
