package org.adnan.carleton.messenger.resource;


import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;

import org.adnan.carleton.messenger.beans.MessageFilterBean;
import org.adnan.carleton.messenger.model.Message;
import org.adnan.carleton.messenger.service.MessageService;


@Path("/messages")
//@Produces(value = {MediaType.APPLICATION_JSON, MediaType.TEXT_XML  })
@Consumes(MediaType.APPLICATION_JSON)
public class MessageResource {

	MessageService messageService = new MessageService();
	
/*	@GET
	public List<Message> getMessages(@QueryParam("year") long year,
									 @QueryParam("start") int start,
									 @QueryParam("size") int size){		
		if(year > 0 ) // if a year was passed as query parameter
			return messageService.getAllMessageByYear(year);
		else if (start >=0 && size>=1)
			return messageService.getAllMessagesPaginated(start, size);
		else
			return messageService.getAllMessages();		
	}
*/		
	@GET
	@Produces(MediaType.TEXT_XML)
	public List<Message> getMessagesXml(@BeanParam MessageFilterBean filterBean){		
		
		System.out.println("Produced Xml");
		if(filterBean.getYear() > 0 ) // if a year was passed as query parameter
			return messageService.getAllMessageByYear(filterBean.getYear());
		else if (filterBean.getStart() >=0 && filterBean.getSize() >=1)
			return messageService.getAllMessagesPaginated(filterBean.getStart() , filterBean.getSize());
		else
			return messageService.getAllMessages();		
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Message> getMessagesJson(@BeanParam MessageFilterBean filterBean){		
		
		System.out.println("Produced Json");
		if(filterBean.getYear() > 0 ) // if a year was passed as query parameter
			return messageService.getAllMessageByYear(filterBean.getYear());
		else if (filterBean.getStart() >=0 && filterBean.getSize() >=1)
			return messageService.getAllMessagesPaginated(filterBean.getStart() , filterBean.getSize());
		else
			return messageService.getAllMessages();		
	}
	@POST
	public Response addMessage(Message m, @Context UriInfo uriInfo) throws URISyntaxException{ 		
		
		// way 3 
		Message newMessage = messageService.addMessage(m);
		URI uri = uriInfo.getAbsolutePathBuilder().path(String.valueOf(m.getId())).build();
		
		return Response.created(uri).entity(newMessage).build();
	//	return newMessage;
	}
	
	@Path("/{msgId}")
	@PUT
	public Message updateMessage(@PathParam("msgId") Long msgId, Message m){
		m.setId(msgId);
		return messageService.updateMessage(m);
	}
	
	@Path("/{msgId}")
	@DELETE
	public Message deleteMessage(@PathParam("msgId") Long msgId){
		return messageService.removeMessage(msgId);
	}
	
	@Path("/{msgId}")
	@GET
	public Message getMessage(@PathParam("msgId") Long msgId, @Context UriInfo uriInfo){
		Message msg = messageService.getMessage(msgId);
		msg.addLink(getSelfUri(uriInfo, msg),"self");
		msg.addLink(getProfileUri(uriInfo, msg),"profile");		
		msg.addLink(getCommentsUri(uriInfo, msg),"comments");			
		return msg;		
	}
	
	private String getCommentsUri(UriInfo uriInfo, Message msg) {
		URI uri = uriInfo.getBaseUriBuilder().path(MessageResource.class).
		path(MessageResource.class,"getCommentResource")
		.path(CommentResource.class)
		.resolveTemplate("msgId",msg.getId())
		.build();
		return uri.toString();
	}	
	
	@Path("/{msgId}/comments")
	public CommentResource getCommentResource(){
		return new CommentResource();	
	}

	private String getSelfUri(UriInfo uriInfo, Message msg) {
		String uri = uriInfo.getBaseUriBuilder().path(MessageResource.class).
		path(Long.toString(msg.getId())).build().toString();
		return uri;
	}	
	
	private String getProfileUri(UriInfo uriInfo, Message msg) {
		String uri = uriInfo.getBaseUriBuilder().path(ProfileResource.class).
		path(msg.getAuthor()).build().toString();
		return uri;
	}
	

	
}
