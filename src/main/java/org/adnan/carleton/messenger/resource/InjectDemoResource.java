package org.adnan.carleton.messenger.resource;

import javax.ws.rs.Consumes;
import javax.ws.rs.CookieParam;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.MatrixParam;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

@Path("/injectdemo")
@Produces(MediaType.TEXT_PLAIN)
@Consumes(MediaType.TEXT_PLAIN)
public class InjectDemoResource {

	@GET
	@Path("/annotations")
	public String getParamsUsingAnnotations(@MatrixParam("aMatrixParam") String aMatrixParam,
											@HeaderParam("myAuthId") String myAuthId,
											@CookieParam ("myCookie") String myCookie){
		return "Matrix Param: " + aMatrixParam +
				" Header Param: " + myAuthId +
				" Cookie Param: " + myCookie;
	}
	
	@GET
	@Path("/context")
	public String getParamsUsingContext(@Context UriInfo uriInfo,@Context HttpHeaders headers){
		return "Path: " + uriInfo.getAbsolutePath().toString() +
				"\nCookies: " + headers.getCookies().toString();
	}
	
}
