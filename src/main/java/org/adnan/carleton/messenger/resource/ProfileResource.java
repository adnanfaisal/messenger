package org.adnan.carleton.messenger.resource;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.adnan.carleton.messenger.model.Profile;
import org.adnan.carleton.messenger.service.ProfileService;

@Path("/profiles")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ProfileResource {	
	ProfileService ms = new ProfileService();		
	@GET
	public List<Profile> getProfiles(){
		return ms.getAllProfiles();		
	}		
	@POST
	public Profile addProfile(Profile m){
			return ms.addProfile(m);
	}		
	@Path("/{profileName}")
	@PUT
	public Profile updateProfile(@PathParam("profileName") String profileName, Profile m){
			m.setProfileName(profileName);
			return ms.updateProfile(m);
	}		
	@Path("/{profileName}")
	@DELETE
	public Profile deleteProfile(@PathParam("profileName") String profileName){
		return ms.removeProfile(profileName);
	}		
	@Path("/{profileName}")
	@GET
	public Profile getProfile(@PathParam("profileName") String profileName){
		return ms.getProfile(profileName);}
}
