package org.adnan.carleton.messenger.resource;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.adnan.carleton.messenger.model.Comment;
import org.adnan.carleton.messenger.model.Message;
import org.adnan.carleton.messenger.service.CommentService;

@Path("/")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class CommentResource {

	private CommentService commentService = new CommentService();
	
	@GET
	public List<Comment> AllgetComments(@PathParam("msgId") long messageId){		
		return commentService.getAllComments(messageId);	
	}
	
	@POST
	public Comment addComment(@PathParam("msgId") long messageId, Comment comment){ 
		return commentService.addComment(messageId, comment);
	}
	
	@Path("/{commentId}")
	@PUT
	public Comment updateMessage(@PathParam("msgId") long msgId, 
			                     @PathParam("commentId")long commentId, Comment comment ){
		comment.setId(commentId);
		return commentService.updateComment(msgId, comment);
	}
	
	@GET
	@Path("/{commentId}")
	public String getComment(@PathParam("commentId") long commentId,
							  @PathParam("msgId") long messageId){
		return "comment no. " + commentId + " for message no.: " + messageId;
	}
	
	@Path("/{commentId}")
	@DELETE
	public Comment deleteComment(@PathParam("msgId") long msgId, @PathParam("commentId") long commentId ){
		return commentService.removeComment(msgId, commentId);
	}

}
