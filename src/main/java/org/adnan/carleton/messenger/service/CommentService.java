package org.adnan.carleton.messenger.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ws.rs.NotFoundException;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.adnan.carleton.database.DatabaseClass;
import org.adnan.carleton.messenger.model.Comment;
import org.adnan.carleton.messenger.model.ErrorMessage;
import org.adnan.carleton.messenger.model.Message;

public class CommentService {
	
	Map<Long, Message> messages = DatabaseClass.getMessages();
	
	public List<Comment> getAllComments(long messageId){	
		Map<Long,Comment> comments = messages.get(messageId).getComments();
		return new ArrayList<Comment>(comments.values());
	}	
	
	public Comment getComment(long messageId, long commentId){
		
		// Not so clean way
		/*
		ErrorMessage em = new ErrorMessage("Message or Comment not found", 404, 
				"http://bitbucket.org/adnanfaisal");
		Response response = Response.status(Status.NOT_FOUND).entity(em).build();
		
		Message message = messages.get(messageId);
		if(message == null) throw new WebApplicationException(response);
		
		Comment comment = message.getComments().get(commentId);
		if(comment == null) throw new WebApplicationException(response);
		return comment;
		*/
		
		// Alternative Clean way
		
		Message message = messages.get(messageId);
		if(message == null) throw new NotFoundException();
		
		Comment comment = message.getComments().get(commentId);
		if(comment == null) throw new NotFoundException();
		return comment;
	}	
	
	public Comment addComment(long messageId, Comment comment){
		Map<Long,Comment> comments = messages.get(messageId).getComments();
		comment.setId(comments.size() + 1);
		comments.put(comment.getId(), comment);
		return comment;
	}	
	
	public Comment updateComment(long messageId, Comment comment){
		if(comment.getId() <= 0) return null;			
		Map<Long,Comment> comments = messages.get(messageId).getComments();
		comments.put(comment.getId(),comment); 
		return comment;
	}	
	
	public Comment removeComment(long messageId, long commentId){
		Map<Long,Comment> comments = messages.get(messageId).getComments();
		return comments.remove(commentId);
	}	
}
