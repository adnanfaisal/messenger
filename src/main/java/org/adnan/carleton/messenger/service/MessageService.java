package org.adnan.carleton.messenger.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.adnan.carleton.database.DatabaseClass;
import org.adnan.carleton.exception.DataNotFoundException;
import org.adnan.carleton.messenger.model.Message;

public class MessageService {

	Map<Long,Message> messages = DatabaseClass.getMessages();
	
	public MessageService(){
		messages.put(1L, new Message(1,"Hello",new Date(), "Adnan"));
		messages.put(2L, new Message(2,"Hi",new Date(2015-1900,9,30), "Faisal"));
			
	}

	public List<Message> getAllMessages(){	
		return new ArrayList<Message>(messages.values());
	}
	
	@SuppressWarnings("null")
	public Message getMessage(long id){
		Message aMessage = messages.get(id);
		if(aMessage == null) throw new DataNotFoundException("Message " + id + " not found");
		else return aMessage;
	}
	
	public Message addMessage(Message m){
		m.setId(messages.size()+1);
		messages.put(m.getId(), m);
		return m;
	}
	
	public Message updateMessage(Message m){
		if(m.getId() <= 0) return null;			
		messages.put(m.getId(),m); 
		return m;
	}
	
	public Message removeMessage(long id){
		return messages.remove(id);
	}
	
	// method for filtering by year 
	public List<Message> getAllMessageByYear(long year){
		List<Message> selectedMessages = new ArrayList<>();
		for(Message msg:messages.values()){
			Calendar cal = Calendar.getInstance();
			cal.setTime(msg.getCreated());
			if(cal.get(Calendar.YEAR) == year)
				selectedMessages.add(msg);
		}
		return selectedMessages;
	}
	
	// method for pagination
	public List<Message> getAllMessagesPaginated(int start, int size){
		List<Message> allMessages = new ArrayList<>(messages.values());
		return allMessages.subList(start, start + size);
	}

	
}
