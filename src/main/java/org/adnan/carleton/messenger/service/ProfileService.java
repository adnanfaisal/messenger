package org.adnan.carleton.messenger.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.adnan.carleton.database.DatabaseClass;
import org.adnan.carleton.messenger.model.Profile;

public class ProfileService {
Map<String,Profile> profiles = DatabaseClass.getProfiles();
	
	public ProfileService(){
		profiles.put("adnan.faisal", new Profile(1,"adnan.faisal",new Date(), "Adnan","Faisal"));
		profiles.put("farahna.islam", new Profile(2,"farhana.islam",new Date(2015,9,30), "Farhana","Islam"));
			
	}

	public List<Profile> getAllProfiles(){	
		return new ArrayList<Profile>(profiles.values());
	}
	
	public Profile getProfile(String id){
		return profiles.get(id);		
	}
	
	public Profile addProfile(Profile m){
		m.setId(profiles.size()+1);
		profiles.put(m.getProfileName(), m);
		return m;
	}
	
	public Profile updateProfile(Profile m){
		if(m.getProfileName().isEmpty()) return null;			
		profiles.put(m.getProfileName(),m); 
		return m;
	}
	
	public Profile removeProfile(String id){
		return profiles.remove(id);
	}

}
